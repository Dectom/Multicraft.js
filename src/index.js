const crypto = require(`crypto`);
const axios = require(`axios`);

class Client {
  constructor(url, user, key) {

    this.options = {url, user, key}

    this.validateOpts();

    /**
     * Time at which the client initiated and classed as ready
     * @type {Date}
     */
    this.readyAt = Date.now();

    return true;
  }

  get uptime() {
    return this.readyAt ? Date.now() - this.readyAt : null;
  }

  async validateOpts(opt = this.options) {
    let tests = [
      {
        pattern: /Incorrect API Key/gi,
        err: 'INVALID_API_KEY'
      },
      {
        pattern: /Invalid API user/gi,
        err: 'INVALID_API_USER'
      }
    ]
    if (!opt.url) throw new Error('MISSING_API_URL');
    if (!opt.key) throw new Error('MISSING_API_KEY');
    if (!opt.user) throw new Error('MISSING_API_USER');
    return await this.sendReq({method: "getCurrentUser"});
    if(!res.success) for (let i in tests) if (tests[i].pattern.test(res.errors[0])) throw new Error(tests[i].err);
    else return true;
  }

  async sendReq({ method, content = {}, location = this.options.url, user = this.options.user, key = this.options.key, data = [] }) {
    var keystr = "";
    var params = content ? content : {};
    params["_MulticraftAPIMethod"] = method;
    params["_MulticraftAPIUser"] = user;
    if (data) {
      for (let i in data) {
        params[data[i].type] = data[i].data;
      }
    }
    for (var param in params) {
      if (!params.hasOwnProperty(param)) continue;
      keystr += param + params[param].toString();
    }
    var hmac = crypto.createHmac('sha256', key);
    hmac.update(keystr);
    var digest = hmac.digest('hex');
    params["_MulticraftAPIKey"] = digest;
    var r = await axios.get(location, { params });
    return r.data;
  }

  /**
   * User Requests
   */

  /**
  * Used list all users
  */
  async listUsers() {
    return await this.sendReq({ method: "listUsers" });
  }

  /**
   * Get any user based on their ID
   * @param {Number} id ID of the User you want to get
   */
  async getUser(id) {
    return await this.sendReq({ method: "getUser", data: [{ type: "id", data: id }] });
  }

  /**
   * Used to get the current user who is authenticating
   */
  async getCurrentUser() {
    return await this.sendReq({ method: "getCurrentUser" });
  }

  /**
   * Used to check the users FTP Access to a given server
   * @param {Number} userID ID of the User you want to check
   * @param {Number} serverID ID of the Server you are wanting to check FTP Access for
   */
  async getUserFtpAccess(userID, serverID) {
    return await this.sendReq({ method: "getUserFtpAccess", data: [{ type: "user_id", data: userID}, {type: "server_id", data: serverID}]});
  }

  /**
   * Used to set a users FTP Access to a given server
   * @param {Number} userID ID of the User you want to check
   * @param {Number} serverID ID of the Server you are wanting to change FTP Access for
   * @param {String} mode The FTP Mode you want to set the user to
   */
  async setUserFtpAccess(userID, serverID, mode) {
    return await this.sendReq({ method: "setUserFtpAccess", data: [{ type: "user_id", data: userID }, { type: "server_id", data: serverID }, {type: "mode", data: mode}] });
  }

  /**
   * Used to check a users role for a server
   * @param {Number} userID ID of the User you want to check
   * @param {Number} serverID ID of the Server you are checking the role for
   */
  async getUserRole(userID, serverID) {
    return await this.sendReq({ method: "getUserRole", data: [{ type: "user_id", data: userID }, { type: "server_id", data: serverID }] });
  }

  /**
   * Used to check a users permissions for a server
   * @param {Number} userID ID of the User you want to check
   * @param {Number} serverID ID of the Server you are checking the permissions for
   */
  async getUserPermissions(userID, serverID) {
    return await this.sendReq({ method: "getUserPermissions", data: [{ type: "user_id", data: userID }, { type: "server_id", data: serverID }] });
  }

  /**
   * Used to set a users permission for a server
   * @param {Number} userID ID of the User you want to get permissions for
   * @param {Number} serverID ID of the Server you are setting the permissions for
   * @param {String} permissionName The permission you want to set
   * @param {Boolean} value The value you want to set it to, true or false
   */
  async setUserPermission(userID, serverID, permissionName, value) {
    return await this.sendReq({ method: "setUserPermission", data: [{ type: "user_id", data: userID }, { type: "server_id", data: serverID }, {type: "action_name", data: permissionName}, {type: "value", data: value}] });
  }

  /**
   * Used to remove a user from a server
   * @param {Number} userID ID of the User you want to remove
   * @param {Number} serverID ID of the server you want to remove the user from
   */
  async removeUserPermissions(userID, serverID) {
    return await this.sendReq({ method: "removeUserPermissions", data: [{ type: "user_id", data: userID }, { type: "server_id", data: serverID }] });
  }

  /**
   * Used to get the ID of a user
   * @param {String} name Name of the user you want to get
   */
  async getUserId(name) {
    return await this.sendReq({ method: "getUserId", data: [{ type: "name", data: name }] });
  }

  async validateUser(name, password) {
    return await this.sendReq({ method: "getUserId", data: [{ type: "name", data: name }, {type: "password", data: password}] });
  }

  /**
   * Used to create a new user
   * @param {String} name Desired username for the user
   * @param {String} email Desired email for the user
   * @param {String} password Desired password for the user
   */
  async createUser(name, email, password) {
    return await this.sendReq({ method: "createUser", data: [{ type: "name", data: name }, { type: "email", data: email }, { type: "password", data: password }] });
  }

  /**
   * Used to delete a user based of ID
   * @param {Number} userID ID of the user you want to delete
   */
  async deleteUser(userID) {
    return await this.sendReq({ method: "deleteUser", data: [{ type: "user_id", data: userID },] });
  }

  /**
   * Server Requests
   */

  /**
  * Can be called to list all servers
  */
  async listServers() {
    return await this.sendReq({ method: "listServers" });
  }

  /**
   * Used to get details about a server
   * @param {Number} id ID of the server you want to get
   */
  async getServer(id) {
    return await this.sendReq({ method: "getServer", data: [{ type: "id", data: id }] });
  }

  /**
   * Used to Start a server
   * @param {Number} id ID of the server you want to start
   */
  async startServer(id) {
    return await this.sendReq({ method: "startServer", data: [{ type: "id", data: id }] });
  }

  /**
   * Used to Stop a server
   * @param {Number} id ID of the server you want to stop
   */
  async stopServer(id) {
    return await this.sendReq({ method: "stopServer", data: [{ type: "id", data: id }] });
  }

  /**
   * Used to Kill a server
   * @param {Number} id ID of the server you want to kill
   */
  async killServer(id) {
    return await this.sendReq({ method: "killServer", data: [{ type: "id", data: id }] });
  }

  /**
   * Used to get a servers Resource Usage
   * @param {Number} id ID of the server you want the resource usage for
   */
  async getServerResources(id) {
    return await this.sendReq({ method: "getServerResources", data: [{ type: "id", data: id }] });
  }

  /**
   * Used to get the log of a server
   * @param {Number} id ID of the server
   */
  async getServerLog(id) {
    return await this.sendReq({ method: "getServerLog", data: [{ type: "id", data: id }] });
  }

  /**
   * Used to clear a servers log
   * @param {Number} id ID of the server
   */
  async clearServerLog(id) {
    return await this.sendReq({ method: "clearServerLog", data: [{ type: "id", data: id }] });
  }

  /**
   * Used to get the Ports from a server
   * @param {Number} id ID of the server
   */
  async listServerPorts(id) {
    return await this.sendReq({ method: "listServerPorts", data: [{ type: "id", data: id }] });
  }

  /**
   * Used to remove an additional port from a server
   * @param {Number} id ID of the server
   * @param {Number} port Port to remove
   */
  async removeServerPort(id, port) {
    return await this.sendReq({ method: "removeServerPort", data: [{ type: "id", data: id }, { type: "port", data: port }] });
  }

  /**
   * Adds a random port to a Server
   * @param {Number} id ID of the server
   */
  async addServerPort(id) {
    return await this.sendReq({ method: "addServerPort", data: [{ type: "id", data: id }] });
  }
};

module.exports = Client;
