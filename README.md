# Multicraft.js

Multicraft.js is a project that was created to make a quick and easy way to interact with the Multicraft API from a NodeJS environment. This should make it easier for us to manage and control how we interact with the API, opening up many more opportunities for how to control and interact with Multicraft.

## Installation

Installation is simple, Just run `npm install multicraft.js`

## Example usage

```js
const Multicraft = require("multicraft.js");
const client = new Multicraft("https://location.of/api.php", "user", "APIKey");

client.getCurrentUser().then(data => {
  console.log(data);
});
```

## Current Methods

This is what has been implemented but there is more to be added.

- getUser(id)
- getCurrentUser()
- getUserRole(userID, serverID)
- createUser(name, email, password)
- deleteUser(userID)
- listServers()
- getServer(id)
- startServer(id)
- stopServer(id)
- killServer(id)
- getServerResources(id)
- getServerLog(id)
- clearServerLog(id)
- listServerPorts(id)
- addServerPort(id)
- removeServerPort(id, port)